import { Component, OnInit } from '@angular/core';
import { CategoriesService } from 'src/app/services/categories.service';
import { ICategory } from 'src/app/interfaces/ICategory';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-categories-edit',
  templateUrl: './categories-edit.component.html',
  styleUrls: ['./categories-edit.component.css']
})
export class CategoriesEditComponent implements OnInit {

  category: ICategory = {};
  
  formCategoryEdit = new FormGroup({
    "name": new FormControl("", Validators.required)
  });


  constructor(
    private cs: CategoriesService,
    private router: ActivatedRoute,
    private routers: Router
  ) { }

  ngOnInit() {
    const categoryId: string = this.router.snapshot.params.id;
    this.cs.getCategory(categoryId).subscribe(data => {
      console.log(data);
      this.category = data;
      this.formCategoryEdit.controls.name.setValue(data.name);

      // this.spinner = false;
    });
  }

  async editCategory() {
    const categoryId: string = this.router.snapshot.params.id;
    await this.cs.updateCategory(categoryId, this.formCategoryEdit.value).toPromise();
    this.routers.navigate(['/categories']);
  }

  async removeCategory() {
    // this.spinner = true;
    const categoryId: string = this.router.snapshot.params.id;
    await this.cs.deleteCategory(categoryId).toPromise();
    this.routers.navigate(['/categories']);
  }

}

