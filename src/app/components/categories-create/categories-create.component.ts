import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CategoriesService } from 'src/app/services/categories.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categories-create',
  templateUrl: './categories-create.component.html',
  styleUrls: ['./categories-create.component.css']
})
export class CategoriesCreateComponent implements OnInit {

  formCategory = new FormGroup({
    "name": new FormControl("", Validators.required)
  });
  // mes = {};

  constructor(
    private cs: CategoriesService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  addCategory() {
    this.cs.createCategory(this.formCategory.value).subscribe(() => {
      this.router.navigate(['/categories']);
    });
  }

}
