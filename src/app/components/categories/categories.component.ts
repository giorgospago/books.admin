import { Component, OnInit } from '@angular/core';
import { CategoriesService } from 'src/app/services/categories.service';
import { ICategory } from 'src/app/interfaces/ICategory';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  categories: ICategory[] = [];

  constructor(
    private cs: CategoriesService
  ) { }

  ngOnInit() {
    this.categories = [];
    // this.spinner = true;

    this.cs.getCategories().subscribe(data => {
      this.categories = data;
      // this.spinner = false;
    });
  }

  async removeCategory(categoryId) {
    await this.cs.deleteCategory(categoryId).toPromise();

    this.cs.getCategories().subscribe(data => {
      this.categories = data;
    });
  }

}
