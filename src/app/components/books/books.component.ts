import { Component, OnInit } from '@angular/core';
import { IBook } from 'src/app/interfaces/IBook';
import { BooksService } from 'src/app/services/books.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  books: IBook[] = [];

  constructor(private bs: BooksService) { }

  ngOnInit() {
    this.books = [];
    // this.spinner = true;

    this.bs.getBooks().subscribe(data => {
      this.books = data;
      // this.spinner = false;
    });
  }

}
