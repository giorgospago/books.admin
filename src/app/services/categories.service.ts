import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ICategory } from '../interfaces/ICategory';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(private http: HttpClient) { }

  getCategories() {
    return this.http.get<ICategory[]>('http://localhost:3000/admin/categories/');
  }

  createCategory(form) {
    return this.http.post("http://localhost:3000/admin/categories/create", form);
  }
  
  getCategory(id) {
    return this.http.get<ICategory>('http://localhost:3000/admin/categories/' + id);
  }

  updateCategory(id, form) {
    return this.http.put('http://localhost:3000/admin/categories/' + id, form);
  }

  deleteCategory(id) {
    return this.http.request('delete', 'http://localhost:3000/admin/categories/' + id);
  }
}
