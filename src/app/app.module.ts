import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeComponent } from './components/home/home.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { CategoriesCreateComponent } from './components/categories-create/categories-create.component';
import { CategoriesEditComponent } from './components/categories-edit/categories-edit.component';
import { BooksComponent } from './components/books/books.component';
import { BooksCreateComponent } from './components/books-create/books-create.component';
import { BooksEditComponent } from './components/books-edit/books-edit.component';

const appRoutes: Routes = [
  {path:'', component: HomeComponent},
  {path:'categories', component: CategoriesComponent},
  {path:'categories/create', component: CategoriesCreateComponent},
  {path:'categories/edit/:id', component: CategoriesEditComponent},
  {path:'books', component: BooksComponent},
  {path:'books/create', component: BooksCreateComponent},
  {path:'books/edit/:id', component: BooksEditComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CategoriesComponent,
    BooksComponent,
    CategoriesCreateComponent,
    BooksCreateComponent,
    CategoriesEditComponent,
    BooksEditComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
